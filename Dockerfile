FROM nginx

RUN mkdir -p /html/default /html/updates

COPY default.conf /etc/nginx/conf.d/default.conf

COPY favicon.ico /html/default
COPY output /html/default

EXPOSE 80
