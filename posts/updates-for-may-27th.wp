<ul>
<li>GPSLog Labs now calculates "Best efforts" for your uploaded
activity. This is a powerful new analysis that finds the fastest
sections of an activity over standard distances and allows them to
be compared easily. For example, it may find that the fastest 10km
of a run was the last portion, and allow that to be compared
against all other times you've run that distance or further.
<div class="p_embed p_image_embed"><a href=
"../post_images/log-stats-bestefforts.png">
<img alt="Log-stats-bestefforts" height="423" src=
"../post_images/log-stats-bestefforts.png.scaled.500.jpg"
width="500"></a></div>
The times for each distance are shown on the Comparisons tab of the
Activity detail page using box plots to compare them to other
activity on the same route or with the same tags.
<p>Activity times are extrapolated if they are close to a standard
distance (within about 10%).</p>
<p>Best efforts replace the old "standard splits" which were locked
to the start of the activity, which were similar but limited in
usefulness.</p>
</li>
<li>Clicking on the best effort distance will show the Best Effort
Detail tab:
<div class="p_embed p_image_embed"><a href=
"../post_images/log-besteffort-detail1.png">
<img alt="Log-besteffort-detail1" height="332" src=
"../post_images/log-besteffort-detail1.png.scaled.500.jpg"
width="500"></a></div>
<p>This shows the detailed statistics for the best effort and
compares it to similar efforts on other activity for the route and
tags.</p>
<p>There's also a map showing the segment of the log where the best
effort was recorded, the fastest sections with and without stops
are coloured blue and green, the whole track being shown in
grey.</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/log-besteffort-detail2.png">
<img alt="Log-besteffort-detail2" height="379" src=
"../post_images/log-besteffort-detail2.png.scaled.500.jpg"
width="500"></a></div>
<p>The best effort is highlighted on the speed graphs too, in the
example below, the best efforts with and without stops
coincide:</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/log-besteffort-detail3.png">
<img alt="Log-besteffort-detail3" height="518" src=
"../post_images/log-besteffort-detail3.png.scaled.500.jpg"
width="500"></a></div>
</li>
<li>The computed best efforts can unfortunately often be incorrect
due to bad GPS data, and while it's possible to clean up the
activity using the Filters, it is also possible to solve this
problem by giving the system an indication of a more reasonable
maximum speed for the best effort.
<p>Click the"Recompute best effort" button on the
detail page for the incorrect best effort:</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/recompute-besteffort2.png">
<img alt="Recompute-besteffort2" height="307" src=
"../post_images/recompute-besteffort2.png.scaled.500.jpg"
width="500"></a></div>
This will recompute the best effort for this activity, but will
ignore any potential best efforts that are above the speed
indicated as reasonable. By lowering the speed you should be able
to find a best effort that ignores sections which are "corrupted"
due to bad GPS data.
</li>
<li>Best efforts are summarised for tags and routes, allowing you
to find your fastest times over given distances:
<div class="p_embed p_image_embed"><a href=
"../post_images/tag-bestefforts.png">
<img alt="Tag-bestefforts" height="232" src=
"../post_images/tag-bestefforts.png.scaled.500.jpg"
width="500"></a></div>
</li>
<li>Best efforts can be visualised for a route too, to give an
indication of where the sections you go fastest are:
<div class="p_embed p_image_embed"><a href=
"../post_images/route-bestefforts-locations.png">
<img alt="Route-bestefforts-locations" height="329" src=
"../post_images/route-bestefforts-locations.png.scaled.500.jpg"
width="500"></a></div>
The altitude profile of the route is superimposed over a chart
indicating the locations of the recorded best efforts at each
distance for activity on the route.
</li>
<li>The Activity Filters editing page has been improved for filters
that discard a lot of points so they are now displayed without
slowing down the page unnecessarily.
</li>
<li><a href=
"http://blog.gpsloglabs.com/using-the-position-filter-to-clean-tracks-thr">
Waypoints</a> have been added to time/manual discard
filter in addition to the position filter. This makes it possible
to quickly and easily select a section of an activity where the GPS
signal is bad and replace it with a more accurate representation of
the track.
</li>
<li>The little clock thumbnails now have an indication of a.m. and
p.m. to help identify activity at a glance:
<div class="p_embed p_image_embed"><img alt="Timethumbnail-am"
height="64" src=
"../post_images/timethumbnail-am.gif"
width="64"></div>
Times are indicated as "a.m." with a thicker line
going from the end time to noon.
<div class="p_embed p_image_embed"><img alt="Timethumbnail-pm"
height="64" src=
"../post_images/timethumbnail-pm.gif"
width="64"></div>
Times are indicated as "p.m." with the thicker line
going from noon to the start time.
</li>
<li>On the <a href="http://gpsloglabs.com/places/network">Organise
Places</a> page, there is a Network tab that shows a neat
visualisation of your activity between the places you have created:
<div class="p_embed p_image_embed"><a href=
"../post_images/places-network.png">
<img alt="Places-network" height="518" src=
"../post_images/places-network.png.scaled.500.jpg"
width="500"></a></div>
</li>
</ul>
