<p>Here's something I've been <a href=
"http://successlessness.blogspot.com/2008/10/merging-gps-logs-and-mapping-them-all.html">
playing with for a while</a> that is now trivially easy using
<a href="http://gpsloglabs.com/">GPSLog labs</a>: Merging GPS log
files into a single KML file for viewing in Google Earth.</p>
<p>You can select a group of logs from any place where there is a
tab of logs (a tag, a route, a date etc.):</p>
<div class="p_embed p_image_embed">
    <img alt="Screenshot-1" height="267" src="../post_images/Screenshot-1.png" width="354">
</div>
<p>1. Click the "Select All" link on top of the table, then click
the yellow "Select All" checkbox that slides out under that.</p>
<p>2. Choose <strong>Google Earth (KML)</strong>, and select the
following options (you must select <strong>Merged</strong>
otherwise you will get a zip file containing the individual
logs):</p>
<div class="p_embed p_image_embed">
    <img alt="Screenshot-3" height="125" src="../post_images/Screenshot-3.png" width="421">
</div>
<p><strong>Simplify</strong>: This will reduce the
number of points in the file by throwing away any points that are
not necessary to define the shape of the tracks. i.e
points along a straight line will be removed leaving only the end
points.</p>
<p><strong>Points</strong>: Output a small marker at
each log point.</p>
<p><strong>Lines</strong>: Output a line connecting
log points.</p>
<p><strong>Floating</strong>: If this is ticked, then
the points will "float" above the ground according to their logged
altitude reading. If it's not ticked, all logs will be
"clamped" to the ground level.</p>
<p>3. When you click <strong>Download Selected Logs</strong> you'll
get a Google Earth file like the screenshot.</p>
<p>It's really cool to zoom in on a road you travel along a lot and
see all the tracks woven together. You can also get
some interesting variations by playing with the various KML
generation options:</p>
<div class="p_embed p_image_embed">
    <a href="../post_images/faq_merged_kml_2.jpg">
        <img alt="Faq_merged_kml_2" height="371" src="../post_images/faq_merged_kml_2.jpg.scaled.500.jpg" width="500">
    </a>
</div>
