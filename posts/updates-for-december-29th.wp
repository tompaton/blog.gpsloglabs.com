<p>A few little enhancements to <a href="http://gpsloglabs.com/">GPSLog Labs</a> that have been made
lately:</p>
<ul>
<li>Many graphs now have a "View full-screen" link like the maps.
This will let you really zoom into the details!
</li>
<li>A few pages on the site now take advantage of wider screens if
you have one.
</li>
<li>Standard splits displayed on the log detail Comparisons tab
now show extrapolated times if they're a
little short (within 10% of the split distance). This is handy if
you've got a route that's pretty close to a standard distance and
you'd like to see how it compares to other routes.<br>
The rank of the activity is displayed in the comparison table too,
and the standard split times are now also displayed on the distance
finish time predictions graph.
</li>
<li>Uploaded log files can now be deleted in bulk from the device
log files page. This is handy if you're logging via an iPhone app
and extra files are included in the zip files you email.
</li>
<li>The trend graphs (for Tags, Goals, Routes) have a line of best
fit added, as well as displaying the overall average.
</li>
<li>The Distance vs Time graphs on the Tag and Goal detail pages
have a new Group by tags option in addition to the group by route
and age options.
</li>
<li>The <a href="http://gpsloglabs.com/places/map/allusers/">map
showing all GPSLog Labs users' places</a> now clusters icons
together and shows how many places have been added in each region.
</li>
<li>The remaining battery life for your device warning
on the home page now gives a range in it's estimation of how much
time is left.</li>
</ul>
