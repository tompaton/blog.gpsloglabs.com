<p><a href="http://gpsloglabs.com/" target="_blank">GPSLog Labs</a>
now has a powerful and customizable filtering feature to clean up
errors in your logs caused by bad GPS signals.</p>
<p>There is a new menu option on the log detail page that will let
you edit the filters applied to that log:</p>
<div class="p_embed p_image_embed"><img alt=
"Screenshot-filter_menu" height="66" src=
"../post_images/Screenshot-filter_menu.png"
width="163"></div>
<p>The following graph comes from a log recorded while running and
shows the kind of noisy data that is often present.
The incorrect speeds mess up the maximum speed,
average speed and distance stats and often correspond to a bad
trace on the map too.</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/Screenshot-unfiltered.png">
<img alt="Screenshot-unfiltered" height="236" src=
"../post_images/Screenshot-unfiltered.png.scaled.500.jpg"
width="500"></a></div>
<p><b>Discard filters</b></p>
<p>The first filters you can choose from will discard invalid
points from your log and then interpolate the speed and course
between the remaining points.</p>
<div class="p_embed p_image_embed"><img alt=
"Screenshot-add_discard" height="369" src=
"../post_images/Screenshot-add_discard.png"
width="416"></div>
<p>After applying those filters, the speed vs time graph looks a
lot better (discarded points are colour-coded to match up with the
chosen filters):</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/Screenshot-discard_only.png">
<img alt="Screenshot-discard_only" height="236" src=
"../post_images/Screenshot-discard_only.png.scaled.500.jpg"
width="500"></a></div>
<p><b>Post filters</b></p>
<p>The second filtering stage applies an average function to the
newly cleaned up data. Smoothing the speed graph will
give you a more reliable average and maximum speed statistic and
also improve the distance and time split data, but won't clean up
the trace on the map or affect the total distance.</p>
<div class="p_embed p_image_embed"><img alt="Screenshot-add_post"
height="221" src=
"../post_images/Screenshot-add_post.png"
width="414"></div>
<p>The original speeds are shown on the graph below as a light blue
line, and after applying a median filter, the thick blue line has
been smoothed (no discard filters were applied for this
graph).</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/Screenshot-post_only.png">
<img alt="Screenshot-post_only" height="236" src=
"../post_images/Screenshot-post_only.png.scaled.500.jpg"
width="500"></a></div>
<p>This graph shows the final results when both discard and post
filters are applied:</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/Screenshot-filtered.png">
<img alt="Screenshot-filtered" height="234" src=
"../post_images/Screenshot-filtered.png.scaled.500.jpg"
width="500"></a></div>
<p><b>Existing logs</b></p>
<p>The new filters will only take affect for new uploads, existing
logs will retain the data from the old filter that GPSLog Labs used
to apply until they are reprocessed.</p>
<p>Also, the discarded points from the old filter will no longer be
shown on the Speed vs Time graph on the log detail other graphs
tab. This information is only available now for logs
using the new filter system from the Filters menu item.</p>
<p><b>Try it out</b></p>
<p>I suggest using a simple Speed Median filter initially, as that
will clean out a lot of noise, setting the window time parameter to
a larger value will smooth the speed more. I'll be
describing the other filters in future posts.</p>
<p>As always, let me know if you have any difficulties or
suggestions, and I hope this new feature helps you get more out of
the site and your GPS logger!</p>
