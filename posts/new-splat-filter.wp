<p>A problem you'll encounter very quickly when using a GPS logger
is "splats" in your log files caused by a bad signal while you're
inside a building or other area with lots of signal
reflections.</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/0Screenshot-splat-filter-before.png">
<img alt="0screenshot-splat-filter-before" height="377" src=
"../post_images/0Screenshot-splat-filter-before.png.scaled.500.jpg"
width="500"></a></div>
<p>This can be very annoying as it will often create spikes in your
speed that completely mess up any maximum speed or distance
statistics for that log file.</p>
<p>For a while, <a href="http://gpsloglabs.com/">GPSLog Labs</a>
has had "position" and "time" filters that let you manually remove
these "splats" but it was tedious to get right, so now a simpler
"splat filter" has been added that makes the process much less
painful.</p>
<p>First, go to the activity you want to filter and click on the
Filters link in the side panel:</p>
<div class="p_embed p_image_embed"><img alt=
"Screenshot-splat-filter-before" height="81" src=
"../post_images/Screenshot-splat-filter-before.png"
width="264"></div>
<p>Then click "Add a filter" and choose "Splat filter":</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/Screenshot-splat-filter-add.png">
<img alt="Screenshot-splat-filter-add" height="608" src=
"../post_images/Screenshot-splat-filter-add.png.scaled.500.jpg"
width="500"></a></div>
<p>On the Splat filter page, you need to drag the marker so that it
covers the "door" of the building. It doesn't have to cover all of
the spikes of the splat, you just need to capture the last good
point before you enter the building and the first good point when
you get out and start getting a good signal again.</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/Screenshot-splat-filter-save.png">
<img alt="Screenshot-splat-filter-save" height="566" src=
"../post_images/Screenshot-splat-filter-save.png.scaled.500.jpg"
width="500"></a></div>
<p>As you can see, the marker is only covering the actual building
under the splat, not the whole splat.</p>
<p>The Splat filter will then discard all the points while you were
inside the building and interpolate over the gap (which will give
you a long period of very slow or 0 speed, restoring the stats of
your log back to something sane):</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/Screenshot-splat-filter-after.png">
<img alt="Screenshot-splat-filter-after" height="660" src=
"../post_images/Screenshot-splat-filter-after.png.scaled.500.jpg"
width="500"></a></div>
<p>The results are dramatically better, the distance and maximum
speed are both now much more accurate and
representative. This log also included the Static
Navigation filter as the AMOD AGL 3080 logger was quite bad for
logging at walking speeds.</p>
<p>Enjoy!</p>
