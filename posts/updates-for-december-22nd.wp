<ul>
<li>The "This week's activity" table on the home page of <a href=
"http://gpsloglabs.com/">GPSLog Labs</a> now allows you to scroll
back and forward through your uploaded activity using 
"&laquo; Previous|This week|Next &raquo;"
links. The old "Earlier activity" link has been removed as it's no
longer needed.
</li>
<li>
The <a href="http://gpsloglabs.com/reports/trends">Trends
report</a> has been optimized and now runs much faster.
</li>
<li>
When editing an automatically created place, a list of suggested
"Nearby places" is displayed to make it easier to correct logs
where your logger didn't lock on at a place:
<div class="p_embed p_image_embed">
<a href=
"../post_images/Screenshot-change-auto-created.png">
<img alt="Screenshot-change-auto-created" height="450" src=
"../post_images/Screenshot-change-auto-created.png.scaled.500.jpg"
width="500"></a></div>
</li>
<li>
When editing an unmatched place, nearby places are now shown as
markers on the map too. Clicking them will select the place from
the drop-down list.
</li>
<li>
The activity detail page heading has been reformatted to make it
easier to read:
<div class="p_embed p_image_embed">
<img alt="Screenshot-activity-detail-hea" height="81" src=
"../post_images/Screenshot-activity-detail-hea.png"
width="438"></div>
</li>
<li>
The libraries of icons you can select from for your tags and places
have been expanded and refined. They should be better looking and
more useful now. Edit your tags or places to change them to one of
the new icons:
<p>Tag icons:</p>
<div class="p_embed p_image_embed">
<img alt="All_tags" height="224" src=
"../post_images/all_tags.png"
width="288"></div>
<p>Place icons:</p>
<div class="p_embed p_image_embed">
<img alt="All_places" height="160" src=
"../post_images/all_places.png"
width="224"></div>
<p>You can still upload your own icons if you want to, for best
results they should be 16x16 pixels, in GIF or PNG format with
transparent backgrounds.</p>
</li>
<li>
You can now add goals based on the built in training programs.
Click the <a href="http://gpsloglabs.com/goals/add/program">Add
Goal from Training Program</a> link on the <a href=
"http://gpsloglabs.com/goals/">Goals</a> page, select the program,
put in your goal finish date (race/event day), select the tag
you'll use to track your training logs and you're ready to go.
</li>
<li>
Two new training programs have been added too:
</li>
<ul>
<li>
<a href="http://gpsloglabs.com/goals/add/program/4/">Beginner Half
Marathon Program</a><br>
A training schedule for beginning runners who want to start racing
the half marathon race distance.
</li>
<li>
<a href="http://gpsloglabs.com/goals/add/program/5/">Intermediate
Marathon Program</a><br>A
training schedule for intermediate runners who want to improve
performance in the marathon race distance.
</li>
</ul>
</li>
<li>Adding tasks to goals has been made faster and easier too, and a
bug was fixed that was preventing adding fractions of kilometers as
the target distances (i.e. you could only enter whole numbers, not
6.5km for example.)</li>
</ul>
<p>As always, feedback and suggestions are welcome and I hope everyone
has a happy and safe Christmas and New Year.</p>
