<p>There's a new Wind Effect graph on the <a href=
"http://gpsloglabs.com/">GPSLog Labs</a> route details Other Graphs
tab.</p>
<p>This graph attempts to quantify the effects of wind from
different directions on logs along a route. It is intended to
assist you to choose a route when bike riding (either to minimise
the effect of the wind or to maximise it if you're keen!)</p>
<p>There are 4 lines plotting the relative effect on the overall
average speed for the route for different wind strengths. The units
are relative and indicative only.</p>
<p>For example, for a log such as the following (predominantly
traveling in an east-west and west-east direction):</p>
<div class="p_embed p_image_embed"><img alt="Route-wind-effect-1a"
height="438" src=
"../post_images/route-wind-effect-1a.png"
width="408"></div>
<p>The computed Wind Effect indicates that the greatest effect will
be when the the wind is from the east or west, and the minimum when
the wind is from the north or south:</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/route-wind-effect-1b.png">
<img alt="Route-wind-effect-1b" height="147" src=
"../post_images/route-wind-effect-1b.png.scaled.500.jpg"
width="500"></a></div>
<p>For a route which has a less symmetrical profile, with long
sections traveling east and south:</p>
<div class="p_embed p_image_embed"><img alt="Route-wind-effect-2a"
height="437" src=
"../post_images/route-wind-effect-2a.png"
width="408"></div>
<p>The computed wind effect is more even, but there is a definite
advantage when the wind is coming from the west:</p>
<div class="p_embed p_image_embed"><a href=
"../post_images/route-wind-effect-2b.png">
<img alt="Route-wind-effect-2b" height="150" src=
"../post_images/route-wind-effect-2b.png.scaled.500.jpg"
width="500"></a></div>
<p>The wind model used is pretty primitive and assumes that a
headwind has the same effect on your speed as a tailwind, but it's
better than nothing. If anyone knows of any more
sophisticated models that could be used, I'd be happy to look into
it.</p>
