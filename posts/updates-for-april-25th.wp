<!-- 
.. link: 
.. description: 
.. tags: changelog, bestefforts, itinerary, race results, activityfeed
.. date: 2013/04/25 17:28:00 +1000
.. title: Updates for April 25th
.. slug: updates-for-april-25th
-->

<p>
    Here are some "new" features in <a href="http://gpsloglabs.com/">GPSLog Labs</a>
    that I hadn't had a chance to post about yet:
</p>
<ul>
    <li>
        <p>
           The <a href="http://gpsloglabs.com/tags/feed">Tag Activity Feed</a>
           is a list of completed and projected distance milestones for each tag,
           such as completing 1000 km (or miles):
        </p>
        <div class="p_embed p_image_embed">
            <a href="../post_images/updates-for-april-25th/milestone-feed.png">
                <img src="../post_images/updates-for-april-25th/milestone-feed.scaled500.png">
            </a>
        </div>
        <p>
           The projections are computed with a Monte Carlo simulation based on your logged activity.
           The graph shows your cumulative distance and the range of likely outcomes, with the
           projected date taken at the median.
        </p>
        <p>
            A selection of these milestones may be displayed on the home page
            below the activity table to let you know of targets you have
            recently completed or will hit soon.
    </li>
    <li>
        <p>
            The log detail page now shows the longest best effort at the top of the page:
        </p>
        <div class="p_embed p_image_embed">
            <a href="../post_images/updates-for-april-25th/longest-best-effort.png">
                <img src="../post_images/updates-for-april-25th/longest-best-effort.scaled500.png">
            </a>
        </div>
        <p>
            This lets you see at a glance how well the activity compares to others
            with the same tag and on the same route, and you can click through to
            view the other distances on the Comparisons tab.
        </p>
    </li>
    <li>
        <p>
            The Virtual Race allows you to see the result of a hypothetical race
            between all your efforts for a particular set of tags.
        </p>
        <div class="p_embed p_image_embed">
            <a href="../post_images/updates-for-april-25th/virtual-race.png">
                <img src="../post_images/updates-for-april-25th/virtual-race.scaled500.png">
            </a>
        </div>
        <p>
            You can find the Virtual Race tab on the tag detail page, and there
            you can select a distance and a target time or speed and the relative positions
            of each effort as the benchmark effort crosses the "finish line" will be shown.
        </p>
    </li>
    <li>
        <p>
            The activity table details section has a cleaner layout:
        </p>
        <div class="p_embed p_image_embed">
            <img src="../post_images/updates-for-april-25th/activity-table.png">
        </div>
    </li>
    <li>
        <p>
            Activity notes popups have been expanded to show more details,
            and you can now have more than one open at a time:
        </p>
        <div class="p_embed p_image_embed">
            <a href="../post_images/updates-for-april-25th/note-popup.png">
                <img src="../post_images/updates-for-april-25th/note-popup.scaled500.png">
            </a>
        </div>
    </li>
    <li>
        <p>
            The Log File detail page has a new Itinerary tab,
            and the Itinerary tabs throughout the site have a nicer, cleaner layout:</p>
        <div class="p_embed p_image_embed">
            <a href="../post_images/updates-for-april-25th/itinerary.png">
                <img src="../post_images/updates-for-april-25th/itinerary.scaled500.png">
            </a>
        </div>
    </li>
    <li>
        <p>
            Also, I've fixed some problems that occurred when uploading zip files
            with non-English file names.
        </p>
    </li>
</ul>
