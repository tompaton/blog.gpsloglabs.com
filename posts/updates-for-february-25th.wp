<p>This series of <a href="http://gpsloglabs.com/" target=
"_blank">GPSLog Labs</a> updates focused on reducing the amount of
clutter on the pages and providing a single place for each
function.</p>
<p><a href="http://gpsloglabs.com/" target="_blank"><strong>Home
page</strong></a></p>
<ul>
<li>The "This Week's Activity" totals have been moved to the
<a href="http://gpsloglabs.com/reports/trends" target=
"_blank"><strong>Reports | Trends</strong></a> page.
</li>
<li>Goal progress for the week has been moved to the
<strong><a href="http://gpsloglabs.com/goals" target=
"_blank">Goals</a></strong> page.</li>
</ul>
<p><a href="http://gpsloglabs.com/uploads/" target=
"_blank"><strong>Uploaded Log Files page</strong></a></p>
<ul>
<li>The list of uploaded log files has been pared down to two
columns and now indicates whether places have been matched
successfully with an icon next to the file-name.
<div class="p_embed p_image_embed"><a href=
"../post_images/0Screenshot-inbox-layout-status.png">
<img alt="0screenshot-inbox-layout-status" height="58" src=
"../post_images/0Screenshot-inbox-layout-status.png.scaled.500.jpg"
width="500"></a></div>
<p>The icon will go green to indicate that the log has been chopped
cleanly:</p>
<div class="p_embed p_image_embed"><img alt=
"Screenshot-inbox-layout-status" height="92" src=
"../post_images/Screenshot-inbox-layout-status.png"
width="321"></div>
<p>The old "quick edit" functionality will reappear in another
incarnation sometime in the future.</p>
</li>
</ul>
<p><strong>Editing Processed Activity for a Log File</strong></p>
<ul>
<li>The Activity column now incorporates a summary of the activity
and the Route, Tag and Notes editing have been moved out of main
interface. You can still add Routes and Tags, but editing is now
done on the Activity detail page.
<div class="p_embed p_image_embed"><a href=
"../post_images/Screenshot-rawlog-results-1.png">
<img alt="Screenshot-rawlog-results-1" height="64" src=
"../post_images/Screenshot-rawlog-results-1.png.scaled.500.jpg"
width="500"></a></div>
</li>
</ul>
<p><strong>Activity Detail page</strong></p>
<ul>
<li>The drop-down menu has been removed and the page heading and
"bread crumbs" simplified:
<div class="p_embed p_image_embed"><img alt=
"Screenshot-log-detail-0" height="152" src=
"../post_images/Screenshot-log-detail-0.png"
width="391"></div>
</li>
<li>To replace the menu, a new information display and editing
panel has been added to right hand side of the page:
<div class="p_embed p_image_embed"><img alt=
"Screenshot-log-detail-panel-0" height="684" src=
"../post_images/Screenshot-log-detail-panel-0.png"
width="257"></div>
</li>
<li>On this new panel, you can see all the details of the log and
also directly edit notes about the activity, add the activity to
Tags and Selections.
</li>
<li>There is a summary of the Route, Filters and Sharing status and
you can click the links to go to a page to edit them. These pages
now also have a more consistent layout including "bread
crumbs".</li>
</ul>
<p>As always, I welcome feedback on these changes and would love to
hear any suggestions you may have to make the site better.</p>
